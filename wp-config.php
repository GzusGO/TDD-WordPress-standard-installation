<?php	
// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/env-config-T.php' ) && ($_SERVER['HTTP_HOST'] == "web1.test.com" )) {
    define( 'WP_LOCAL_TEST', true );
    define( 'FORCE_SSL_LOGIN', false );
    define( 'FORCE_SSL_ADMIN', false );
    define( 'WP_CACHE', false ); //Added by WP-Cache Manager back true Gzus
    $urlFolder='/wp';
    include( dirname( __FILE__ ) . '/env-config-T.php' );
} elseif ( file_exists( dirname( __FILE__ ) . '/env-config-D.php' )) {
    define( 'WP_LOCAL_DEV', true );
    define( 'FORCE_SSL_LOGIN', false );
    define( 'FORCE_SSL_ADMIN', false );
    define( 'WP_CACHE', false ); //Added by WP-Cache Manager back true Gzus
    $urlFolder='/wp';
    include( dirname( __FILE__ ) . '/env-config-D.php' );
} else {
    // production
    // TODO: cambiar a por defecto localhost
    $urlFolder='/wp'; 
    define( 'WP_LOCAL_PRO', false );	
    define( 'FORCE_SSL_LOGIN', true );
    define( 'FORCE_SSL_ADMIN', true );
    define( 'WP_CACHE', true ); //Added by WP-Cache Manager back true
    define( 'DB_NAME', '%%DB_NAME%%' );/** The name of the database for WordPress */
    define( 'DB_USER', '%%DB_USER%%' );/** MySQL database username */
    define( 'DB_PASSWORD', '%%DB_PASSWORD%%*' );/** MySQL database password */
    define('DB_HOST', 'localhost');/** MySQL hostname */

}


// increase PHP upload memory limit
define( 'WP_MEMORY_LIMIT', '512M' );
//deleate trash
define( 'EMPTY_TRASH_DAYS', 30 ); // 30 days
//The entries below were created by iThemes Security to enforce SSL
define( 'FORCE_SSL_LOGIN', true );
define( 'FORCE_SSL_ADMIN', true );
define( 'WP_POST_REVISIONS', 10 );


// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'].'/content' );
define( 'WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'].$urlFolder );
define( 'WP_HOME', 'http://' . $_SERVER['HTTP_HOST'].$urlFolder );
define( 'WP_PLUGIN_DIR', dirname( __FILE__ ) .'/content/plugins' );
define( 'UPLOADS', 'http://' . $_SERVER['HTTP_HOST'].'/upload' );


// ================================================
// You almost certainly do not want to change these
// ================================================
define( 'DB_CHARSET', 'utf8' );
define( 'DB_COLLATE', '' );
// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY','');
define('SECURE_AUTH_KEY','');
define('LOGGED_IN_KEY','');
define('NONCE_KEY','');
define('AUTH_SALT','');
define('SECURE_AUTH_SALT','');
define('LOGGED_IN_SALT','');
define('NONCE_SALT','');
// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// ==============================================================
$table_prefix  = 'er7b9i_';
// ================================
// Language
// Leave blank for American English
// ================================
define( 'WPLANG', '' );
// ===========
// Hide errors
// ===========
ini_set( 'display_errors', 0 );
define( 'WP_DEBUG_DISPLAY', false );
// =================================================================
// Debug mode
// Debugging? Enable these. Can also enable them in local-config.php
// =================================================================
// define( 'SAVEQUERIES', true );
// define( 'WP_DEBUG', true );
// ======================================
// Load a Memcached config if we have one
// ======================================
if ( file_exists( dirname( __FILE__ ) . '/memcached.php' ) )
	$memcached_servers = include( dirname( __FILE__ ) . '/memcached.php' );
// ===========================================================================================
// This can be used to programatically set the stage when deploying (e.g. production, staging)
// ===========================================================================================

define( 'WP_STAGE', '%%WP_STAGE%%' );
define( 'STAGING_DOMAIN', '%%WP_STAGING_DOMAIN%%' ); // Does magic in WP Stack to handle staging domain rewriting

// ===================
// Bootstrap WordPress
// ===================
if ( !defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/wp/' );

require_once( ABSPATH . "wp-settings.php" );
