# The diamonds dreams - Wordpress standard installation

A simple way to work, track issues, update and develop WordPress with git. And with support for different environments.

## The problem vs. solution:

If you are doing a lot of development on WordPress, you probably have run on many issues each time you change form one environment to other. 

### Migration dependency:

This is a big problem on WordPress development because it depends on the root address, so you have to hack the DB and run some script to re-calculate all routes on the installation. Also, every folder in the installation has the same problem. So this methodology and repo try to solve or alleviate that separating the WordPress files from your work folders for the development of themes and plugins. In most of the cases, you should change the positions of the core files so you can make it independent. 
    
### Environmentally aware: 
    
Usually, there is a different setting for each environment. For example,  it can be that in production you probably want to have some cache. But in development that is not a good idea, If you're going to see your changes at the moment of developing. In testing and development, you probably want some log option on that is not needed on production. 
    
You can set an environmental specific configuration file for this:
    
- env-config-T.php :: test
- env-config-D.php :: development 
- env-config-P.php :: production
    
We also include a script that read the server environment variables and the domain name. Knowing where it is, to set the configuration for your different environment automatically. So with this technique and repo, you only worry about your code regardless of your flow between environment without changing setting each time you migrate the project from env A to env B. 
     
### Git Traking: 

Migration dependencies regularly make it very difficult to use the ```git pull``` method for performing an update or track your changes from the different environment (Development, testing, and production) in a standard Wordpress installation. So a solution of that is using our proposed methodology.  So you can push from your dev, and the Dev-op server takes care of sync your changes in testing and production. 

### One Standard One way: 
    
The best way to save time and make sure that everything is ready to go. It's having a standard and a template from where you can copy and paste want you to need to solve a problem without thinking much about it this. That is what this repo intent to do. 

Here it is just the best way to install and git track code on WordPress projects we can think on. 
    
We hope you can contribute with this, and make this part of your tools set if you are a WordPress developer. 

## installation 

- Go to your root folder or working folder ``` cd ROOT_FOLDER ```
- Clone this repo ``` git clone REPO_URL ```
- Create the DB in MySql 
- use one of this two configuration methods: variables on file or Variable on the server. 

### a) Variables on File

Edit the environmental configuration files for each of your environment:

- env-config-T.php :: test
- env-config-D.php :: development 
- env-config-P.php :: production
    
Remember to add the database connection information and the domain name of the different server you are using http:// or https:// at the beginning for ex: for development (http://localhost:8080, http://mySite.dev/ ), http://test.mysite.com, http://mySite.com. 

### b) Constant on Server 

In case you have access to the server environmental constants 

## Configurations: 

## Way of use:

### Wordpress Operations

Dividing the core code from the working folder give us many advantages we would try to show how to take some of this to you

#### Update Worpress

You can update the last version and changes on the WordPress official repo with only: 

- Go to the WP folder Ex: ``` cd wp ``` 
- pull the last changes: ``` git pull ```

#### Change version of Wordpress

The Wordpress repo use git tags for saving the deferent versions of WordPress so if you need to test your theme or plugin in deferents versions of WordPress you can use this feature:

- See all versions available: ``` git tag ```
- Change version ``` git checkout 4.6 ```

### Standard Themes

This repo came already with two WordPress standard Theme twentyseventeen and twentysixteen also can be updated with git doing this:

- go to the theme folder ``` cd  ontent/themes/twentyseventeen ``` 
- update the theme using git ``` git pull ```

### Custome Theme 
    
You should use the working folder ``` ontent/themes/ ``` to add your new project.
    
#### Create a git trackable custom theme

we recommend creating the repo of your theme project at first and using submodule to getting the install for example:

add your repo to the theme folder using git submodule: ```
git submodule add https://github.com/YOUR_ACCOUNT/YOUR_REPO content/themes/YOUR_THEME_FOLDER ```

That same technique can be used on gitlab or bitbucket.

#### Create a git theme update
 
 So now you can get to your working folder and work as usual with git ``` cd content/themes/YOUR_THEME_FOLDER ```.

### Custome plug-ins
#### Create a git trackeble custoem plug-ins
#### Create a git plug-ins update
### Third parties Plug-ins
### Media Folders 
### git base trackables servers:
#### gitlab
#### github
#### bitbucket


## Base on: 

- [wordPress github](https://github.com/WordPress/WordPress)
- [WordPress wp-config](https://codex.wordpress.org/Editing_wp-config.php)
- [WordPress Themes twentyseventeen](https://github.com/WordPress/twentyseventeen)
- [WordPress Themes twentysixteen](https://github.com/WordPress/twentysixteen)
- [WordPress-Skeleton](https://github.com/markjaquith/WordPress-Skeleton)
- [WordPress-Skeleton video ](https://www.youtube.com/watch?v=dSMb6H_0v6E)
- [WordPress white spaces fix for xml and rss](http://wejn.org/stuff/wejnswpwhitespacefix.php.html)
